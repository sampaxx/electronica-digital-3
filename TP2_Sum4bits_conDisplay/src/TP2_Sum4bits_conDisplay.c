/*

===============================================================================
 Name        : TP2_Sum4bits_conDisplay.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================


#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>

void Init(void);

#define AddrFIO0DIR 0x2009C000
//#define AddrFIO1DIR 0x2009C020

#define AddrFIO0PIN 0x2009C014 //Definido como salida en la funcion Init()
#define AddrFIO2PIN 0x2009C054 //Definido como entrada por defecto


unsigned int volatile *const FIO0DIR = (unsigned int*) AddrFIO0DIR;
//unsigned int volatile *const FIO1DIR = (unsigned int*) AddrFIO1DIR;
unsigned int volatile *const FIO0PIN = (unsigned int*) AddrFIO0PIN;
unsigned int volatile *const FIO2PIN = (unsigned int*) AddrFIO2PIN;

int main(void){
	Init();
	while (1){
		if (*FIO2PIN & 0x00001000){//Se hace la suma cuando el P1_31 esta en alto
			*FIO0PIN = (0x0000000f & *FIO2PIN) + ((0x000000f0 & *FIO2PIN)>>4);
		}
	}
	int resultado = 6;
}
void Init (void)
{
	*FIO0DIR = 0x0000000f; // Setea GPIO - P0_0:P0_3 como salidas
}
*/

/*
===============================================================================
 Name        : displays.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif
#include <cr_section_macros.h>

#define AddrFIO0DIR 0X2009C000
#define AddrFIO2DIR 0X2009C040
#define AddrFIO0SET 0X2009C018
#define AddrFIO0CLR 0X2009C01C
#define AddrPIN4SEL 0x4002C010
#define AddrFIO2PIN 0x2009C054
#define addrFIO0PIN 0x2009C014
#define FIO0PIN ((unsigned int volatile*) addrFIO0PIN)

unsigned int volatile * const FIO0DIR=(unsigned int *) AddrFIO0DIR ;
unsigned int volatile * const FIO2DIR=(unsigned int *) AddrFIO2DIR ;
unsigned int volatile * const FIO0SET=(unsigned int *) AddrFIO0SET ;
unsigned int volatile * const FIO0CLR=(unsigned int *) AddrFIO0CLR ;
unsigned int volatile * const PINSEL4=(unsigned int *) AddrPIN4SEL ;
unsigned int volatile * const FIO2PIN=(unsigned int *) AddrFIO2PIN ;

int swich=1; //  0= numero ingresado por llave. 1=numero ingresado por llave
int variable = 0b1101;
int unidad;
int decena;
int u1;
int d1;
int u;
int d;
int tabla[]=  {
		0b11000000,
		0b11111001,
		0b01011011,
		0b10110000,
		0b10011001,
		0b10010010,
		0B10000011,
		0B11111000,
		0B00000000,
		0B10011000
		};

void config(void);
void delay(void);

int main(void) {

config();
unidad=variable%10;
decena=variable/10;


    while(1) {

    	void leer(void);
    	if(swich==0){u=u1; d=d1;}
    	if(swich==1){u=unidad; d=decena;}

    	*FIO0SET|=3; 				// apaga los dos displays
    	*FIO2PIN= tabla[u];	//saco por el puerto 2 las unidades, y activo display 1.
    	*FIO0SET|=2;
    	*FIO0CLR|=1;
    	delay();
       	*FIO0SET|=3; 				// apaga los dos displays
       	*FIO2PIN= tabla[d]; 	//saco por el puerto 2 las decenas, y activo display 2.
       	*FIO0SET|=1;
       	*FIO0CLR|=2;
       	delay();


    }
    return 0 ;
}


void config(void)
//por defecto(reset):
// FIOMASK=0
// PINMODE=0 (Pull Up)
// PINMODE_OD=0 (salida en modo normal, no open drain)
{	*PINSEL4 = 0x000000;  //Configura los pines del PORT2 como GPIO;
	*FIO2DIR = 0xffffffff;  //Configura el PORT2 como salida;
	*FIO0DIR =3; // Configura pin 0y1 del PORT0 como salida, y el resto como entradas;

}

void leer(void){
int var=0;
	if ((*FIO0PIN &(1<<6)) == (1<<6) ) { var|=0b0001;}
	if ((*FIO0PIN &(1<<7)) == (1<<7) ) { var|=0b0010;}
	if ((*FIO0PIN &(1<<8)) == (1<<8) ) { var|=0b0100;}
	if ((*FIO0PIN &(1<<9)) == (1<<9) ) { var|=0b1000;}

	u1=var%10;
	d1=var/10;
	}

void delay(void){
	for (int i=10000;i>0;i--){}
	}


