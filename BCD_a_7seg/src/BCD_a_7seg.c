/*
 * Por defecto los pines de los puertos GPIO (luego de un reset), estan configurados como:
 * GPIO (00 en los PINxSEL)
 * pull-up habilitado (00 en PINxMODE)
 * no open drain (0 en PINxMODE_OD)
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>

void Init(void);

#define AddrFIO2DIR 0x2009C040
#define AddrFIO2PIN 0x2009C054 //Definido como salida en la funcion Init()
#define AddrFIO2SET 0x2009C058
#define AddrFIO2CLR 0x2009C05C
#define AddrFIO0DIR 0X2009C000
#define AddrFIO0PIN 0x2009C014

unsigned int volatile *const FIO2DIR = (unsigned int*) AddrFIO2DIR;
unsigned int volatile *const FIO2PIN = (unsigned int*) AddrFIO2PIN;
unsigned int volatile *const FIO2SET = (unsigned int*) AddrFIO2SET;
unsigned int volatile *const FIO2CLR = (unsigned int*) AddrFIO2CLR;
unsigned int volatile *const FIO0DIR = (unsigned int*) AddrFIO0DIR;
unsigned int volatile *const FIO0PIN = (unsigned int*) AddrFIO0PIN;


int main(void){
	Init();

unsigned int tabla[]= {
									// posicion de los segmentos
									// a b c d e f g h
					0x000000c0,		//0 - 11111100
			  	  	0x000000f9,		//1 - 11111001
					0x000000a4,		//2 - 10100100
					0x000000b0,		//3 - 10110000
					0x00000099,		//4 - 10011001
					0x00000012,		//5 - 00010010
					0x00000082,		//6 - 10000010
					0x000000f8,		//7 - 11111000
					0x00000080,		//8 - 10000000
					0x00000090};	//9 - 10010000


	unsigned int decena=~tabla[0];	//LS Display ; tener en cuenta que los valores estan negados ("coso de la ñ")
	unsigned int unidad=~tabla[0];	//MS Display
	unsigned int resultado; // Valor a mostrar en los displays



	while(1){

		resultado=((*FIO0PIN & 0x000003C0)>>6) + ((*FIO0PIN & 0x00078000)>>15);

		decena=~tabla[resultado%10];      // estan son las decenaes, el operador %, da como resultado el resto de la division
		unidad=~tabla[(resultado/10)%10]; // estan son las unidad

	for(int i=0;i<100000;i++){
		*FIO2PIN = decena;
		*FIO2CLR = (1<<8);
		*FIO2SET &= (1<<10);
	}
	for(unsigned int i=0;i<100000;i++){
		*FIO2PIN = unidad;
		*FIO2CLR = (1<<10); //enciendo el unidad?
		*FIO2SET &= (1<<8);  //apago el decena?
	}


	}
}

void Init (void)
{
	*FIO2DIR = 0x000005ff; 		//Setea GPIO - P0_0:P0_7 como salidas del display y PO_8//PO_9 para MPX
	*FIO0DIR = ~0b00000000010001111000001111000000;	 //Setea GPIO0  como entradas
}
