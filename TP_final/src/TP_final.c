/*
===============================================================================
  Name        : tpfinal.c
 Author      : Candotti, Enzo - D'Andrea, David
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>
#include <math.h>

unsigned int volatile * const ISER0 = (unsigned int *) 0xE000E100;
unsigned int volatile * const ICER0 = (unsigned int *) 0xE000E180;
unsigned int volatile * const PCONP = (unsigned int *) 0x400FC0C4;
unsigned int volatile * const IO0IntEnF = (unsigned int *) 0x40028094;
unsigned int volatile * const IO0IntClr = (unsigned int *) 0x4002808C;
unsigned int volatile * const IO0IntStatF = (unsigned int *) 0x40028088;

//ADC punteros
unsigned int volatile * const AD0CR = (unsigned int *) 0x40034000;
unsigned int volatile * const AD0INTEN = (unsigned int *) 0x4003400C;
unsigned int volatile * const AD0GDR = (unsigned int *) 0x40034004;
unsigned int volatile * const AD0DR2 = (unsigned int *) 0x40034018;
unsigned int volatile * const AD0DR1 = (unsigned int *) 0x40034014;
unsigned int volatile * const AD0DR0 = (unsigned int *) 0x40034010;

//TIMER0
unsigned int volatile * const T0CTCR = (unsigned int *) 0x40004070;
unsigned int volatile * const T0MR0 = (unsigned int *) 0x40004018;
unsigned int volatile * const T0MR1 = (unsigned int *) 0x4000401C;
unsigned int volatile * const T0PR = (unsigned int *) 0x4000400C;
unsigned int volatile * const T0MCR = (unsigned int *) 0x40004014;
unsigned int volatile * const T0TCR = (unsigned int *) 0x40004004;
unsigned int volatile * const T0IR = (unsigned int *) 0x40004000;
unsigned int volatile * const T0TC=(unsigned int*) 0x40004008;
unsigned int volatile * const T0PC=(unsigned int*) 0x40004010;

//GPIO   puerto 0
unsigned int volatile * const FIO0DIR = (unsigned int *) 0x2009C000;
unsigned int volatile * const FIO0SET = (unsigned int *) 0x2009C018;
unsigned int volatile * const FIO0CLR = (unsigned int *) 0x2009C01C;
unsigned int volatile * const FIO0PIN = (unsigned int *) 0x2009C014;
//GPIO puerto2
unsigned int volatile * const FIO2DIR = (unsigned int *) 0x2009C040;
unsigned int volatile * const FIO2SET = (unsigned int *) 0x2009C058;
unsigned int volatile * const FIO2CLR = (unsigned int *) 0x2009C05C;
unsigned int volatile * const FIO2PIN = (unsigned int *) 0x2009C054;

//PINSEL
unsigned int volatile * const PINSEL0 = (unsigned int *) 0x4002C000;
unsigned int volatile * const PINSEL1 = (unsigned int *) 0x4002C004;
unsigned int volatile * const PINSEL2 = (unsigned int *) 0x4002C008;
unsigned int volatile * const PINSEL3 = (unsigned int *) 0x4002C00C;
unsigned int volatile * const PINSEL4 = (unsigned int *) 0x4002C010;

//PINMODE
unsigned int volatile * const PINMODE0 = (unsigned int *) 0x4002C040;
unsigned int volatile * const PINMODE1 = (unsigned int *) 0x4002C044;
unsigned int volatile * const PINMODE2 = (unsigned int *) 0x4002C048;
unsigned int volatile * const PINMODE3 = (unsigned int *) 0x4002C04C;
unsigned int volatile * const PINMODE4 = (unsigned int *) 0x4002C050;

//CLOCK
unsigned int volatile * const PCLKSEL0 = (unsigned int *) 0x400FC1A8;

//UART3
unsigned int volatile * const U3RBR = (unsigned int *) 0x4009C000;
unsigned int volatile * const U3DLL = (unsigned int *) 0x4009C000;
unsigned int volatile * const U3DLM = (unsigned int *) 0x4009C004;
unsigned int volatile * const U3IER = (unsigned int *) 0x4009C004;
unsigned int volatile * const U3LCR = (unsigned int *) 0x4009C00C;
unsigned int volatile * const U3LSR = (unsigned int *) 0x4009C014;
unsigned int volatile * const U3THR = (unsigned int *) 0x4009C000;

//Prototipos
void configADC(void);
void configUART(void);
void configTimer(void);
float calculoTemp(int);
float calculoRes(int);
void enviar(void);

int b1,b2,b3;
int temp,r1,r2,r3; //variables utilizadas para la obtención de los datos de la temperatura de los sensores
int flag=0;
int c1=0;
int c2=0;
int c3=0;
#define muestras_tomadas 500
#define muestras_enviadas 50
int unidades,decenas;
int temp_deseada=25; //Temperatura a la cual se quiere llegar
int recepcion[2];// Vector para guardar los datos enviados desde la PC para obtener valores de display y temperatura deseada
int cont=0;
int cont1=0;

unsigned int numDisplay[]= {
									// posicion de los segmentos
									// a b c d e f g h
					0x000000c0,		//0 - 11111100
			  	  	0x000000f9,		//1 - 11111001
					0x000000a4,		//2 - 10100100
					0x000000b0,		//3 - 10110000
					0x00000099,		//4 - 10011001
					0x00000012,		//5 - 00010010
					0x00000082,		//6 - 10000010
					0x000000f8,		//7 - 11111000
					0x00000080,		//8 - 10000000
					0x00000090};	//9 - 10010000

int displaySelec; //Variable para seleccionar el display a encender
int indice = 0; //Variable utilizada en la parte de UART0 para manejar los datos que llegan (controla el vector recepcion)
int muestras1[muestras_tomadas],  muestras2[muestras_tomadas],  muestras3[muestras_tomadas];


//Comienzo función principal
int main(void) {

configADC();
configUART();
configTimer();


displaySelec=0;
*FIO0DIR |=(1<<6)|(1<<7)|(1<<8)|(1<<9)|(0<<11)|(1<<16)|(1<<17)|(1<<18)|(1<<23); //Pines para multiplexado,control de calor, coolers
*FIO2DIR |=(1 << 0)|(1<<1)|(1 <<2)|(1<<3)|(1<<4)|(1<<5)|(1<<6);//pines de salida para display (segmentos)

 while(1) {


	if(temp_deseada>temp)
	{
		*FIO0CLR|=(1<<22); //Activo secador  // P0.22 pin 24 placa
		*FIO0CLR|=(1<<16);  //apago cooler   // P0.16 pin 14 placa
	}
	else if(temp_deseada<=(temp)){

		*FIO0SET|=(1<<22); //apago secador
		*FIO0SET|=(1<<16); //activo cooler
	}

    }
    return 0 ;
}//Fin función principal

void configTimer(void){

	*T0MR0 = 50000;// Match interrumpe cada 2mS
	*T0MCR |= (3<<0); //reseteo TC cuando llega la interrupcion
	*T0TC = 0; //TC y PC en 0 (no es necesario ya que vienen por defecto en ese valor)
	*T0PC = 0;
	*T0PR = 0; //Prescaler en 0
	*ISER0= 1<<1; //Interrupcion TIMER0 habilitada
	*T0TCR=1; //Empieza la cuenta

}

void configADC(void){

	*PCONP |= (1<<12); //Habilitamos el power/clock del ADC (no es necesario ya que vienen por defecto en ese valor)
	*AD0CR |= (1<<21); // ADC modo operacional
	*AD0CR |= (1<<0)|(1<<1)|(1<<2); //seleccionamos los canales 0, 1 y 2 para tomar muestras y hacer las conversiones
	*PINSEL1|= (1<<14); // P0.23 = 01  selecciona AD0.0
	*PINSEL1|= (1<<16); // P0.24 = 01  pin 16 selecciona AD0.1
	*PINSEL1|= (1<<18); // P0.25 = 01  selecciona AD0.2
	*PCLKSEL0|=(0b11<<24); //selecciona pclock_adc =cclock/8= 12.5Mhz
	*AD0CR|= (1<<16);	//habilita modo burst
	*AD0INTEN&=~(1<<8); //si burst=1, AD0INTEN debe estar en 0.

}

void configUART(void){
 	*PCONP |= (1<<25);		//Prender UART3
	*U3LCR |= 11;			//Palabra de 8 bits
	*U3LCR |= (1<<7);		//Activar DLAB (Enable access to division latches para config de BR)
	*U3DLL |= 0xA1; 		//DLL= 161 ->9585 baudios
	*U3DLM |= 0;			//DLM=0
	*U3LCR &=~(1<<7);		//Habilita la interrupcion
	*PINSEL0 |= 0xA;		//p0.0 TX y p0.1 RX
	*PINMODE0 |= 0x8;		//Dejar al aire RX
	*U3IER |= 0x1;			//Setea que se produzca una interrupcion
	*ISER0 |= (1<<8);		//Habilita la interrupcion en NVIC
}


void TIMER0_IRQHandler(){
	r1= ((0x0000fff0 & *AD0DR0)>>4);
	r2= ((0x0000fff0 & *AD0DR1)>>4);
	r3= ((0x0000fff0 & *AD0DR2)>>4);

	int res1,res2,res3,temp1,temp2,temp3;
	res1 =calculoRes(r1);
	temp1=calculoTemp(res1);
	if (temp1>-10 && temp1<110 && c1<muestras_tomadas){
		muestras1[c1]=temp1;
		c1++;
		}

	res2 =calculoRes(r2);
	temp2=calculoTemp(res2);
	if (temp2>-10 && temp2<110 && c2<muestras_tomadas){ muestras2[c2]=temp2; c2++;}

	res3 =calculoRes(r3);
	temp3=calculoTemp(res3);
	if (temp3>-10 && temp3<110 && c3<muestras_tomadas){ muestras3[c3]=temp3; c3++;}


	if(c1==muestras_tomadas  && c2==muestras_tomadas && c3==muestras_tomadas)
	{ c1=0; c2=0; c3=0;
		int a1=0;int a2=0;int a3=0;
		for(int i=0;i<muestras_tomadas;i++){
			a1=a1+muestras1[i];
			a2=a2+muestras2[i];
			a3=a3+muestras3[i];
		}
		b1=(a1/muestras_tomadas);
		b2=(a2/muestras_tomadas);
		b3=(a3/muestras_tomadas);

		temp= (b1+b2+b3)/3;
		decenas=temp/10;
		unidades=temp%10;

	}

	switch(displaySelec)  //Manejo de displays (multiplexado y carga de valores)
		{
		case 0:
			*FIO2PIN = ~numDisplay[decenas];
			*FIO2CLR = (1<<8);
			*FIO2SET &= (1<<10);
			displaySelec = 1;  // cambio selector de displays para que en la próxima interrupción habilite el otro display
			break;

		case 1:
			*FIO2PIN = ~numDisplay[unidades];
			*FIO2CLR = (1<<10); //enciendo el unidad?
			*FIO2SET &= (1<<8);  //apago el decena?
			displaySelec = 0;	// cambio selector de displays para que en la próxima interrupción habilite el otro display
			break;
		}


	if(flag==1){
		cont++;

		if (cont==250){   //cada 0,5 segundos envia el estado de los 3 sensores
			cont=0;
			cont1++;
			enviar();
		}
		if (cont1==muestras_enviadas)   //se envian 20 datos
		{
			cont1=0;
			flag=0;
		}
	}

	*T0IR=1;// Bajamos bandera Timer0
}

float calculoRes(int result){

	float  tension1 = result*3.3/4095;  // aca me independizo del valor de  tensión y lo convierto a resistencia
	float VRL=tension1;
	float VR1 =0.0;
	float R1=100000;
	float RL=0.0;
	float I=0.0;
	float VCC=3.3;
	VR1= VCC-VRL;
	I=VR1/R1;
	RL=VRL/I;
	return RL;

}



float calculoTemp(int res){

	//Coeficientes de temperatura del modelo Steinhart-Hart
	float A=0.6279938628E-3;
	float B=2.253256029E-4;
	float C=0.8385906461E-7;

	//modelo de conversion de la resistencia a temperatura
	float temperatura = log(res);
	temperatura = 1 / (A + (B * temperatura) + (C * temperatura * temperatura * temperatura));
	temperatura = temperatura - 273.15;  // Kelvin a grados centigrados

	long x = 10 * temperatura;
	float y = (float)x / (float)10;
	return y;
}


void UART3_IRQHandler(){

	int rec = *U3RBR;
	if(rec>=48 && rec<=57) //Llego un numero entre 0 y 9?
	{
		recepcion[indice]=rec; //Guarda los valores en el arreglo recepcion según la variable indice
		//obtengo los valores que tienen que mostrar los display
		if(indice==1) //llegaron 2 datos?
		{
			temp_deseada = (recepcion[0]-48)*10 + recepcion[1]-48; //Obtiene valor de temperatura deseada a la cual se quiere llegar
			indice=0; //Se resetea el valor indice

			if(temp_deseada>45 || temp_deseada<15 ){
				//En caso de ingresar un valor mayor a 45 se carga 25°C como temperatura por defecto
				temp_deseada=30;// Temperatura cooler por defecto
			}
		}
		else if(indice==0) //Si hay un elemento en el arreglo, llego un solo valor
		{
			indice=1;
		}
	}

	if(flag==0){flag=1;}
}


//Función encargada de mostrar las temperaturas en la ventana del programa Hércules
void enviar(){

	int d1=b1/10;
	int u1=b1%10;
	int d2=b2/10;
	int u2=b2%10;
	int d3=b3/10;
	int u3=b3%10;

	//Mensaje de muestra de temperatura en PC
	int PC[20]={84,49,61,d1+48,u1+48,44,32,84,50,61,d2+48,u2+48,44,32,84,51,61,d3+48,u3+48,10};   //Muestra la temperatura de los 3 sensores

	for(int i=0;i<20;i++){
		while((*U3LSR&(1<<5))==0){}
		*U3THR =PC[i];    // Se envia el dato mediante comunicación serie
	}
}
