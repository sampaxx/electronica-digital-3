.syntax unified
.global led_off
.type led_off, function

led_off:	LDR	R1,FIO3SET		// r1=&FIO3SET cargo en r1 la dir de memoria con la que voy a trabajar
			STR	R0,[R1]			// *r1=r0 guardo el dato que recibi como parametro en la direccion de memoria del fio3set
			MOV pc, lr			// mover reg a reg => pc=lr-1   vuelvo el contador de programa a la instruccion siguiente del llamado a subrutina

FIO3SET:		.word	0x2009C078
