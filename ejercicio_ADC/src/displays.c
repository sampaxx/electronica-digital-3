#include "displays.h"

unsigned int volatile *const FIO2DIR = (unsigned int*) AddrFIO2DIR;
unsigned int volatile *const FIO2PIN = (unsigned int*) AddrFIO2PIN;
unsigned int volatile *const FIO2SET = (unsigned int*) AddrFIO2SET;
unsigned int volatile *const FIO2CLR = (unsigned int*) AddrFIO2CLR;
unsigned int volatile *const FIO0DIR = (unsigned int*) AddrFIO0DIR;
unsigned int volatile *const FIO0PIN = (unsigned int*) AddrFIO0PIN;

void  config_displays(){

	*FIO2DIR |= 0x000005ff; 		//Setea GPIO - P0_0:P0_7 como salidas del display y PO_8//PO_9 para MPX
	*FIO0DIR |= ~0b00000000010001111000001111000000;	 //Setea GPIO0  como entradas
}
