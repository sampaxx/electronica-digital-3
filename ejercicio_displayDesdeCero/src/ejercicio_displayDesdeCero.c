/*
===============================================================================
 Name        : ejercicio_displayDesdeCero.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>

//Definicion de punteros

//UART
unsigned int volatile *const U0LCR= (unsigned int *) 0x4000C00C;
unsigned int volatile *const U0DLL= (unsigned int *) 0x4000c000;
unsigned int volatile *const U0DLM= (unsigned int *) 0x4000c004;
unsigned int volatile *const PINSEL0=(unsigned int *) 0x4002c000;
unsigned int volatile *const PINMODE0=(unsigned int *) 0x4002c040;
unsigned int volatile *const U0IER=(unsigned int *) 0x4000c004;
unsigned int volatile *const U0RBR=(unsigned int *) 0x4000c000;
unsigned int volatile *const U0THR=(unsigned int *) 0x4000c000;

//NVIC
unsigned int volatile *const ISER0=(unsigned int *) 0xe000e100;
unsigned int volatile *const ICER0=(unsigned int *) 0xe000e180;
unsigned int volatile *const IPR0=(unsigned int *) 0xe000e400;

//PCLK
unsigned int volatile * const PCLKSEL0 = (unsigned int *) 0x400fc1a8;

//TIMER
unsigned int volatile * const T0TCR=(unsigned int *) 0x40004004;
unsigned int volatile *const T0CCR=(unsigned int *) 0x40004028;
unsigned int volatile *const T0CR0=(unsigned int *) 0x4000402c;
unsigned int volatile *const T0IR=(unsigned int *) 0x40004000;
unsigned int volatile *const T0MCR=(unsigned int *) 0x40004014;
unsigned int volatile *const T0MR0=(unsigned int *) 0x40004018;

//PCB
unsigned int volatile * const PINSEL3=(unsigned int *) 0x4002c00c;
unsigned int volatile *const PINMODE3=(unsigned int *) 0x4002c04c;

//GPIO
unsigned int volatile *const FIO2DIR=(unsigned int *) 0x2009c040;
unsigned int volatile *const FIO2PIN=(unsigned int *) 0x2009c054;
unsigned int volatile *const FIO2MASK=(unsigned int *) 0x2009c050;
unsigned int volatile *const FIO2SET=(unsigned int *) 0x2009c058;
unsigned int volatile *const FIO2CLR=(unsigned int *) 0x2009c05c;

//prototipos
void config_TIMER();
void config_PINES();

//variables globales
int decena=3, unidad=0;
int tabla[]={// orden de los segmentos g f e d c b a

		0b1111110, // cero
		0b0110000, // uno
		0b1101101, // dos
		0b1111001, // tres 1011110
		0b0110011, // cuatro
		0b1011011, // cinco
		0B1011111, // seis
		0B1110000, // siete
		0B1111111, // ocho
		0B1111011};// nueve
int mux=0b001;

int main(void) {

	config_TIMER();
	config_PINES();
    // TODO: insert code here

    // Force the counter to be placed into memory
    volatile static int i = 0 ;
    // Enter an infinite loop, just incrementing a counter
    while(1) {
        i++ ;
    }
    return 0 ;
}

void config_PINES(){
	*FIO2DIR=0x57f;
}

void config_TIMER(){
	*T0TCR|=1; //habilito la cuenta del timer, por defecto esta encendido
	//el pclk por defecto es cclk/4= 25 MHz
	*T0MCR|=(0b11<<0); // habilita int por MR0 y reseteo el TC al matchear
	*ISER0|=(1<<1); // habilita en NVIC int por timer0
	*T0MR0=500000000; // el timer matchea con MR0 cada 2ms
}

void TIMER0_IRQHandler(){
	//lo que hago cada 2ms
	if(mux==0){
		mux=1;
		*FIO2PIN=tabla[unidad];
		*FIO2SET|=(1<<8); //enciendo el display de las unidades
		*FIO2CLR&=0; //apago el disp de las decenas
	}
	else if(mux==1){
		mux=0;
		*FIO2CLR|=(1<<8);
		*FIO2PIN=tabla[decena];
		*FIO2SET|=(1<<10);
	}
}


