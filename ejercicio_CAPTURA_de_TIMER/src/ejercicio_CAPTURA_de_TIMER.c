/*
===============================================================================
 Name        : ejercicio_CAPTURA_de_TIMER.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>

//Definicion de punteros

//UART
unsigned int volatile *const U0LCR= (unsigned int *) 0x4000C00C;
unsigned int volatile *const U0DLL= (unsigned int *) 0x4000c000;
unsigned int volatile *const U0DLM= (unsigned int *) 0x4000c004;
unsigned int volatile *const PINSEL0=(unsigned int *) 0x4002c000;
unsigned int volatile *const PINMODE0=(unsigned int *) 0x4002c040;
unsigned int volatile *const U0IER=(unsigned int *) 0x4000c004;
unsigned int volatile *const U0RBR=(unsigned int *) 0x4000c000;
unsigned int volatile *const U0THR=(unsigned int *) 0x4000c000;

//NVIC
unsigned int volatile *const ISER0=(unsigned int *) 0xe000e100;
unsigned int volatile *const ICER0=(unsigned int *) 0xe000e180;
unsigned int volatile *const IPR0=(unsigned int *) 0xe000e400;

//PCLK
unsigned int volatile * const PCLKSEL0 = (unsigned int *) 0x400fc1a8;

//TIMER
unsigned int volatile *const T0TCR=(unsigned int *) 0x40004004;
unsigned int volatile *const T0CCR=(unsigned int *) 0x40004028;
unsigned int volatile *const T0CR0=(unsigned int *) 0x4000402c;
unsigned int volatile *const T0IR=(unsigned int *) 0x40004000;

//PCB
unsigned int volatile * const PINSEL3=(unsigned int *) 0x4002c00c;
unsigned int volatile *const PINMODE3=(unsigned int *) 0x4002c04c;

//prototipos de funciones
void config_TIMER0();

// TODO: insert other include files here

// TODO: insert other definitions and declarations here

//variables globales
unsigned int tiempo_cap;

int main(void) {

    // TODO: insert code here

    // Force the counter to be placed into memory
    volatile static int i = 0 ;
    // Enter an infinite loop, just incrementing a counter
    while(1) {
        i++ ;
    }
    return 0 ;
}

void config_TIMER0(){
	//1- despues de un reset, el timer0 y timer1 estan encendidos por defecto
	//   ademas, para que el TC se incremente, deber estar habilitado: T0TCR(0)=1
	*T0TCR|=1; //habilito la cuenta del TC

	//2- PCLK por defecto es CCLK/4=25 MHz

	//3- seleccionar los pines del micro relacionados a este modulo en "pinsel de PCB"
	*PINSEL3|=(0b11<<20); //selecciono el pin p1.26 como cap0.0
	*PINMODE3|=(0b11<<20); //selecciono una resistencia de pulldown en el pin1.26 que es cap0.0

	//4- habilitar las interrupciones del modulo en NVIC y configurar MCR o CCR segun sea necesario
	*ISER0|=(1<<1); //habilito la int de timer0 en NVIC
	*T0CCR|=(0b101); //habilito int por evento en cap0.0 y que se guarde el TC en CR0 cuando ocurra el evento

	//5- config DMA (no lo usamos)

	//6- cargar los registros MRn si se usan
}

void TIMER0_IRQHandler(){
	*T0IR|=(1<<4); //bajo el flag del evento ocurrido en cap0.0 (pin 1.26)
	tiempo_cap=*T0CR0;
}
