/*
===============================================================================
 Name        : Ejercicio_dienteDeSierra_DAC_UART_TIMER.c
 Author      : DAVID
 Version     :
 Copyright   :
 Description :
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>
//#include <math.h>

//Definicion de punteros

//UART
unsigned int volatile *const U0LCR= (unsigned int *) 0x4000C00C;
unsigned int volatile *const U0DLL= (unsigned int *) 0x4000c000;
unsigned int volatile *const U0DLM= (unsigned int *) 0x4000c004;
unsigned int volatile *const PINSEL0=(unsigned int *) 0x4002c000;
unsigned int volatile *const PINMODE0=(unsigned int *) 0x4002c040;
unsigned int volatile *const U0IER=(unsigned int *) 0x4000c004;
unsigned int volatile *const U0RBR=(unsigned int *) 0x4000c000;
unsigned int volatile *const U0THR=(unsigned int *) 0x4000c000;

//NVIC
unsigned int volatile *const ISER0=(unsigned int *) 0xe000e100;
unsigned int volatile *const ICER0=(unsigned int *) 0xe000e180;
unsigned int volatile *const IPR0=(unsigned int *) 0xe000e400;

//PCLK
unsigned int volatile *const PCLKSEL0 = (unsigned int *) 0x400fc1a8;

//TIMER
unsigned int volatile *const T0TCR=(unsigned int *) 0x40004004;
unsigned int volatile *const T0CCR=(unsigned int *) 0x40004028;
unsigned int volatile *const T0CR0=(unsigned int *) 0x4000402c;
unsigned int volatile *const T0IR=(unsigned int *) 0x40004000;
unsigned int volatile *const T0MCR=(unsigned int *) 0x40004014;
unsigned int volatile *const T0MR0=(unsigned int *) 0x40004018;
unsigned int volatile *const T0MR1=(unsigned int *) 0x4000401c;
unsigned int volatile *const T0TC=(unsigned int *) 0x40004008;

//PCB
unsigned int volatile *const PINSEL3=(unsigned int *) 0x4002c00c;
unsigned int volatile *const PINSEL1=(unsigned int *) 0x4002c004;
unsigned int volatile *const PINMODE3=(unsigned int *) 0x4002c04c;

//DAC
unsigned int volatile *const DACR=(unsigned int *) 0x4008c000;

//prototipos
void config_general();
void config_UART();
void config_TIMER();
void enviar(char);

// TODO: insert other include files here

// TODO: insert other definitions and declarations here
unsigned int t1AmpMax;
unsigned int t2periodo;
unsigned int escalon;
unsigned int trama;


int main(void) {

    // TODO: insert code here
	trama=0;
	t1AmpMax=0b1100000000;
	t2periodo=25000000;
	escalon=(t1AmpMax>>5);
	*T0TC=0;
	config_general();
	config_TIMER();
	config_UART();
    // Force the counter to be placed into memory
    volatile static int i = 0 ;
    //int trama;
    // Enter an infinite loop, just incrementing a counter
    while(1) {
        i++ ;
        escalon=(t1AmpMax>>5);
        *T0MR1=t2periodo;
        //*T0MR0=(*T0MR1>>5); // MR0=MR1/32

    }
    return 0 ;
}

void config_general(){
	*IPR0=(0b10<<11); //pongo la prioridad de la interrupcion del timer0 en 2
	*PINSEL1|=(0b10<<20); //pin p0.26 como AOUT
}

void config_UART(){
	*PCLKSEL0|=(0b11<<6); //peripheral clock uart0=cclock/8= 12,5 MHz
	//config del baudrate
	*U0LCR|=(1<<7); // Divisor Latch Access Bit (DLAB)=1
	*U0DLL=81;
	*U0DLM=0;
	*U0LCR&=~(1<<7); // Divisor Latch Access Bit (DLAB)=0
	// como el error es menor al 0,5% no se va a configurar el Fractional Divider
	*PINSEL0|=(0b0101<<4); //el pin P0.2 es TXD0 y p0.3 es RXD0
	*PINMODE0|=(0b10<<6); //p0.3 de RXD0 en repeater mode
	*U0IER|=(0b1<<0); // bit0: int por dato recibido, bit0=0 deshablitada, bit0=1 habilitada
	*ISER0|=(1<<5); //habilito int por uart0 en NVIC
}

void config_TIMER(){
	//1- PCONP: por defecto luego de un reset timer0 viene encendido
		 *T0TCR=1;
	//2- PPCLK: por defecto viene configurado como CCLK/4= 25 MHz
	//3- PINES: en este programa no se usan pines asociados al timer
	//4- Interrupciones:
		 *ISER0|=(1<<1); // hablito int por timer0 en NVIC
		 *T0MCR|=(1<<0); // habilito int por MR0
		 *T0MCR|=(1<<3); // habilito int por MR1
	//5- carga de los MRx
		 *T0MR0=(1<<31); // valor arbitrario grande inicial
		 *T0MR1=25000000; // valor inicial: 1segundo
}

//HANDLERS de las interrupciones
void UART0_IRQHandler(){
	if (trama==0){
		trama=1;
		t1AmpMax=(*U0RBR<<2); //con esto se tiene un valor de 10bits
	}
	else if(trama==1){
		trama=2;
		t2periodo=(*U0RBR<<18);
	}
	else if(trama==2){ //es la tercera trama que llega
		trama=0;
		int temp=(*U0RBR & 0xff);
		t2periodo=t2periodo|(temp<<10);
	}
}

void TIMER0_IRQHandler(){
	int cero=(*T0IR & (1<<0));
	int uno=(*T0IR & (1<<1));
	if((*T0IR & (1<<0))==1){ // int de MR0
		*T0IR|=(1<<0); // limpio flag MR0
		int temp=(*DACR>>6);
		temp=temp+escalon;
		*DACR=(temp<<6);
		*T0MR0=*T0MR0+(*T0MR1>>5);
	}
	if ((*T0IR & (1<<1))==0b10){
		*T0IR|=(1<<1); // limmpio flag MR1
		*DACR=0;
		*T0TC=0;
		*T0MR0=(*T0MR1>>5); //vuelvo el MR0 a su valor inicial
	}
}


