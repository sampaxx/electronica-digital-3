#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>
#include <math.h>
#include "displays.h"
#include "displays.c"

#define AddrPCONP		0x400FC0C4	//
#define AddrPINSEL0		0x4002C000	//
#define AddrU3LCR 		0x4009C00C	//
#define AddrPINMODE0 	0x4002C040
#define AddrU3DLL 		0x4009C000	//
#define AddrU3DLM 		0x4009C004	//
#define AddrU3IER 		0x4009C004	//
#define AddrU3RBR  		0x4009C000	//
#define AddrU3THR  		0x4009C000	//
#define AddrISER0  		0xE000E100
#define AddrU3LSR 		0x4009C014	//
#define AddrAD0CR		0x40034000	//AD0CR - 0x4003 4000
#define AddrPCLKSEL0    0x400FC1A8  //peripheral clock selection
#define AddrPINSEL1		0x4002C004  //PINSEL1
//----------------------------------------------------------------
#define AddrAD0GDR 		0x40034004 // Registro de datos globales del ADC
#define AddrAD0INTEN 	0x4003400C // reg de habilitacion de interrupciones de ADC
#define AddrAD0DR0		0x40034010 // reg de resultado de conversion del canal 0
#define AddrAD0DR1		0x40034014 // reg de resultado de conversion del canal 1
#define AddrAD0DR2		0x40034018 // reg de resultado de conversion del canal 2
//----------------------------------------------------------------
#define AddrFIO2DIR		0x2009C040 // pines del puerto 2 como entrada o salida


unsigned int volatile *const PCONP = (unsigned int*) AddrPCONP;
unsigned int volatile *const PINSEL0 = (unsigned int*) AddrPINSEL0;
unsigned int volatile *const U3LCR = (unsigned int*) AddrU3LCR;
unsigned int volatile *const PINMODE0 = (unsigned int*) AddrPINMODE0;
unsigned int volatile *const U3DLL = (unsigned int*) AddrU3DLL;
unsigned int volatile *const U3DLM = (unsigned int*) AddrU3DLM;
unsigned int volatile *const U3IER = (unsigned int*) AddrU3IER;
unsigned int volatile *const U3RBR = (unsigned int*) AddrU3RBR;
unsigned int volatile *const U3THR = (unsigned int*) AddrU3THR;
unsigned int volatile *const U3U3THR = (unsigned int*) AddrU3THR;
unsigned int volatile *const ISER0 = (unsigned int*) AddrISER0; //bit8 ISE_UART3 UART3 Interrupt Enable.
unsigned int volatile *const U3LSR = (unsigned int*) AddrU3LSR;
unsigned int volatile *const AD0CR = (unsigned int*) AddrAD0CR;
unsigned int volatile *const PCLKSEL0=(unsigned int*) AddrPCLKSEL0;
unsigned int volatile *const PINSEL1=(unsigned int*) AddrPINSEL1;
unsigned int volatile *const AD0GDR=(unsigned int *) AddrAD0GDR;
unsigned int volatile *const AD0INTEN=(unsigned int *) AddrAD0INTEN;
unsigned int volatile *const AD0DR0=(unsigned int *) AddrAD0DR0;
unsigned int volatile *const AD0DR1=(unsigned int *) AddrAD0DR1;
unsigned int volatile *const AD0DR2=(unsigned int *) AddrAD0DR2;
//unsigned int volatile *const FIO2DIR=(unsigned int *) AddrFIO2DIR;

void config(void);
float calculoTemp(int res);

int result;
int resistencia;
int temp;

int main(void) {

	config();
	config_displays();
    volatile static int i = 0;
    unsigned int tabla[]= {
    									// posicion de los segmentos
    									// a b c d e f g h
    					0x000000c0,		//0 - 11111100
    			  	  	0x000000f9,		//1 - 11111001
    					0x000000a4,		//2 - 10100100
    					0x000000b0,		//3 - 10110000
    					0x00000099,		//4 - 10011001
    					0x00000012,		//5 - 00010010
    					0x00000082,		//6 - 10000010
    					0x000000f8,		//7 - 11111000
    					0x00000080,		//8 - 10000000
    					0x00000090};	//9 - 10010000

    unsigned int decena=~tabla[0];	//LS Display ; tener en cuenta que los valores estan negados ("coso de la ñ")
    unsigned int unidad=~tabla[0];	//MS Display
    unsigned int resultado; 		// Valor a mostrar en los displays


    while(1) {
        i++ ;

        decena=~tabla[temp/10];      // estan son las decenaes, el operador %, da como resultado el resto de la division
        unidad=~tabla[(temp/10)/10]; // estan son las unidad

        for(int i=0;i<10000;i++){
        		*FIO2PIN = decena;
        		*FIO2CLR = (1<<8);
        		*FIO2SET &= (1<<10);
        	}
        	for(unsigned int i=0;i<10000;i++){
        		*FIO2PIN = unidad;
        		*FIO2CLR = (1<<10);  //enciendo el unidad?
        		*FIO2SET &= (1<<8);  //apago el decena?
        	}
    }
    return 0;
}

void ADC_IRQHandler(void){

	result=((*AD0DR0 & 0xfff0)>>4);                       // guardo el valor digital de la tension introducida en el canal AD0.0
	float tension=result*3.3/4095;

	/*resistencia=2*(100000*result/4096);                 // aca me independizo del valor de  tensión y lo convierto a resistencia
	float res=resistencia;
	float tempe=temp;
	temp=calculoTemp(resistencia);*/

	float VRL=tension;
	float VR1=0.0;
	float R1=100000;
	float RL=0.0;

	float I=0.0;

	float VCC=3.3;

	VR1=VCC-VRL;
	I=VR1/R1;
	RL=VRL/I;
	float res=RL;
	float temp=calculoTemp(RL);
}

//funcion de calculo de temperatura con el modelo  de Steinhart_Hart
//http://www.thinksrs.com/downloads/programs/Therm%20Calc/NTCCalibrator/NTCcalculator.htm
float calculoTemp(int res){

	//Coeficientes de temperatura del modelo Steinhart-Hart
	float A=0.6279938628E-3;
	float B=2.253256029E-4;
	float C=0.8385906461E-7;

	//modelo de conversion de la resistencia a temperatura
	float temperatura = log(res);
	temperatura = 1 / (A + (B * temperatura) + (C * temperatura * temperatura * temperatura));
	temperatura = temperatura - 273.15;  // Kelvin a grados centigrados

	//long x=temperatura*1000;
	//float y = (float)x / (float)y;
	long x = 10 * temperatura;
	float y = (float)x / (float)10;
	int unidad=y/10;

	return y;
	//return temperatura;
}



void config(){

	//configuracion del modulo ADC

	*PCONP=(1<<12);        //se enciende el modulo adc ya que por default viene apagado
	*AD0CR|=(1<<21);       //segunda habilitacion necesaria del adc
	*AD0CR|=(1<<16);	   // el ADC funciona en modo BURST
	*AD0CR|=0x1;		   // dentro del modulo ADC se habilita para conversión el canal-0
	*PCLKSEL0|=(0b11<<24); //selecciono el clock del adc como core clock/8 = 12.5Mhz
	*PINSEL1|=(0b01<<14);  //selecciono el pin p0.23 en modo entrada de AD0.0 (entrada0 del ADC)
	*ISER0|=(1<<22);	   // se habilitan las interrupciones por ADC en NVIC poniendo en 1 el bit22
	*AD0INTEN=0b000000001; // se habilita interrupcion por fin de conversión del canal 0
	//*AD0INTEN=~(1<<8);   // el modo BURST del ADC requiere el bit AD0GINTEN=0

	//configuracion de los pines de salida
	*FIO2DIR=0b11111;

}
