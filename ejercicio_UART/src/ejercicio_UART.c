/*
===============================================================================
 Name        : ejercicio_UART.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
 tercera prueba de commit
===============================================================================
*/


#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>

#define AddrISER0 0xE000E100     //Interrupt Set Enable Register
#define AddrT0TC 0x40004008      //T0TC - 0x4000 4008
#define AddrT0MR0 0x40004018   	 //T0MR0 - 0x4000 4018
#define AddrT0MCR 0x40004014	 //T0MCR - 0x4000 4014
#define AddrIO0IntEnF 0x40028094 //(IO0IntEnF - address 0x4002 8094)
#define AddrIO2IntEnF 0x400280B4 //(IO2IntEnF - 0x4002 80B4)
#define AddrT0MR1 0x4000401C     //T0MR1 - 0x4000401C
#define AddrT0IR 0x40004000      //T0IR - 0x40004000
#define AddrFIO3DIR 0x2009C060
#define AddrFIO3SET 0x2009C078 //FIO3SET - 0x2009 C078
#define AddrFIO3CLR 0x2009C07C
#define AddrFIO3PIN 0x2009C074
#define AddrT0TCR 0x40004004
#define AddrISER0 0xE000E100
#define AddrT0TCR 0x40004004
#define AddrIOIntStatus 0x40028080	//IOIntStatus - 0x4002 8080
#define AddrIO0IntClr 0x4002808C	//(IO0IntClr - 0x4002 808C)
#define AddrIO2IntClr 0x400280AC	//O2IntClr - 0x4002 80AC
#define AddrPCONP 0x400FC0C4 		//(PCONP - address 0x400F C0C4)
#define AddrU3LCR 0x4009C00C		//U3LCR - 0x4009 C00C
#define AddrU3DLL 0x4009C000 		//U3DLL - 0x4009C000
#define AddrU3DLM 0x4009C004		//U3DLM - 0x4009C004
#define AddrU3IER 0x4009C004		//U3IER - 0x4009C004
#define AddrPINSEL0 0x4002C000		//PINSEL0 0x4002C000


unsigned int volatile *const T0TC=(unsigned int *const) AddrT0TC;
unsigned int volatile *const T0MR0=(unsigned int *const) AddrT0MR0;
unsigned int volatile *const T0MCR=(unsigned int *const) AddrT0MCR;
unsigned int volatile *const IO0IntEnF=(unsigned int *const) AddrIO0IntEnF;
unsigned int volatile *const IO2IntEnF=(unsigned int *const) AddrIO2IntEnF;
unsigned int volatile *const T0MR1=(unsigned int *const) AddrT0MR1;
unsigned int volatile *const T0IR=(unsigned int *const) AddrT0IR;
unsigned int volatile *const FIO3DIR=(unsigned int *const) AddrFIO3DIR;
unsigned int volatile *const FIO3SET=(unsigned int *const) AddrFIO3SET;
unsigned int volatile *const FIO3CLR=(unsigned int *const) AddrFIO3CLR;
unsigned int volatile *const FIO3PIN=(unsigned int *const) AddrFIO3PIN;
unsigned int volatile *const ISER0=(unsigned int *const) AddrISER0;
unsigned int volatile *const T0TCR=(unsigned int *const) AddrT0TCR;
unsigned int volatile *const IOIntStatus=(unsigned int *const) AddrIOIntStatus;
unsigned int volatile *const IO0IntClr=(unsigned int *const) AddrIO0IntClr;
unsigned int volatile *const IO2IntClr=(unsigned int *const) AddrIO2IntClr;
unsigned int volatile *const PCONP= (unsigned int *const) AddrPCONP;
unsigned int volatile *const U3LCR= (unsigned int *const) AddrU3LCR;
unsigned int volatile *const U3DLL= (unsigned int *const) AddrU3LCR;
unsigned int volatile *const U3DLM= (unsigned int *const) AddrU3DLM;
unsigned int volatile *const U3IER= (unsigned int *const) AddrU3IER;
unsigned int volatile *const PINSEL0= (unsigned int *const) AddrPINSEL0;

void config(void);

int main(void) {

    // TODO: insert code here

    // Force the counter to be placed into memory
    volatile static int i = 0 ;
    // Enter an infinite loop, just incrementing a counter
    while(1) {
        i++ ;
    }
    return 0 ;
}

void config(){
	*PCONP|=(1<<25);	//aca se enciende el UART3
	*U3LCR|=(1<<7);		//con esto se accede a los registros DLL y DLM
	*U3DLL=161;			//esto configura el baudrate en 9585 baudios con un error de 0.14...%
	*U3DLM=0;			// "

	*U3LCR &= 0b01111111; //aca deshabilitamos el acceso a DLM y DLL pero habilitamos acceso a THR y RBR
	*U3IER=1;			// Enables the Receive Data Available interrupt for UART3
	*ISER0=(1<<8);		// UART3 Interrupt Enable
	*PINSEL0=0b1010;






}
