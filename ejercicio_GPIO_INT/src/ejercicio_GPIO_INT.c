#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>

// TODO: insert other include files here

// punteros
unsigned int volatile *const ISER0 = (unsigned int *) 0xE000E100;
unsigned int volatile *const IO0IntClr = (unsigned int *) 0x4002808c;
unsigned int volatile *const IO0IntEnR = (unsigned int *) 0x40028090;
//GPIO   puerto 0
unsigned int volatile * const FIO0DIR = (unsigned int *) 0x2009C000;
unsigned int volatile * const FIO0SET = (unsigned int *) 0x2009C018;
unsigned int volatile * const FIO0CLR = (unsigned int *) 0x2009C01C;
unsigned int volatile * const FIO0PIN = (unsigned int *) 0x2009C014;
//GPIO puerto2
unsigned int volatile * const FIO2DIR = (unsigned int *) 0x2009C040;
unsigned int volatile * const FIO2SET = (unsigned int *) 0x2009C058;
unsigned int volatile * const FIO2CLR = (unsigned int *) 0x2009C05C;
unsigned int volatile * const FIO2PIN = (unsigned int *) 0x2009C054;

//prototipos
void config_GPIO();


int main(void) {
	config_GPIO();

    // Force the counter to be placed into memory
    volatile static int i = 0 ;
    // Enter an infinite loop, just incrementing a counter
    while(1) {
        i++ ;
    }
    return 0 ;
}

void config_GPIO() {
	*ISER0|=(1<<21); 	//habilito las int por gpio compartidas con el handler int ext3
	*IO0IntEnR=(1<<0); 	//las int se produciran por flanco de subida en p0.0
	*FIO0DIR=(1<<22); 	//el pin p0.22 es salida (el led rojo)
	*FIO0PIN=0;
}

void EINT3_IRQHandler(){
	if((*FIO0PIN & (1<<22))==1){
		*FIO0CLR=(1<22);
	}
	else if((*FIO0PIN & (1<<22))==0){
		*FIO0SET=(1<22);
	}
	//bajo el flag de esta int
	*IO0IntClr=1;

}
