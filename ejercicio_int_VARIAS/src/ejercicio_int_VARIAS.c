#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>

#define AddrISER0 0xE000E100   //Interrupt Set Enable Register
#define AddrPINSEL4 0x4002C010
#define AddrEXTINT 0x400FC140
#define AddrEXTMODE 0x400FC148 //(EXTMODE - 0x400F C148)
#define AddrEXTPOLAR 0x400FC14C
#define AddrFIO3DIR 0x2009C060
#define AddrFIO3SET 0x2009C078 //FIO3SET - 0x2009 C078
#define AddrFIO3CLR 0x2009C07C
#define AddrFIO3PIN 0x2009C074
#define AddrT0MR0 0x40004018   // Timer0 Match Register0 - 0x4000 4018
#define AddrT0MCR 0x40004014   // Match Control Register
#define AddrT0TCR 0x40004004   // Timer Control Register
#define AddrT0IR 0x40004000    // Timer0 Int. Register
#define AddrFIO0DIR 0x2009C000
#define AddrT0TC 0x40004008   //Timer0 TCounter

unsigned int volatile *const PINSEL4 = (unsigned int *const) AddrPINSEL4;
unsigned int volatile *const ISER0= (unsigned int *const) AddrISER0;
unsigned int volatile *const EXTINT= (unsigned int *const) AddrEXTINT;
unsigned int volatile *const EXTMODE=(unsigned int *const) AddrEXTMODE;
unsigned int volatile *const EXTPOLAR= (unsigned int *const) AddrEXTPOLAR;
unsigned int volatile *const FIO3DIR=(unsigned int *const) AddrFIO3DIR;
unsigned int volatile *const FIO3SET=(unsigned int *const) AddrFIO3SET;
unsigned int volatile *const FIO3CLR=(unsigned int *const) AddrFIO3CLR;
unsigned int volatile *const FIO3PIN=(unsigned int *const) AddrFIO3PIN;
unsigned int volatile *const T0MR0=(unsigned int *const) AddrT0MR0;
unsigned int volatile *const T0MCR=(unsigned int *const) AddrT0MCR;
unsigned int volatile *const T0TCR=(unsigned int *const) AddrT0TCR;
unsigned int volatile *const T0IR=(unsigned int *const) AddrT0IR;
unsigned int volatile *const FIO0DIR=(unsigned int *const) AddrFIO0DIR;
unsigned int volatile *const T0TC=(unsigned int *const) AddrT0TC;



void init(void);

int main(void){
	init();
	unsigned int i=0;
	while(1){
		i++;

	}
}

void init(){
	*PINSEL4=(01<<20);    	   // 21:20=01 selecciona el pin p2.10 como EINT0 (receptor de int ext)
	*ISER0=(0x00040002);       // el bit18=1 habilita interrupciones externas por EINT0
							   // el bit1=1 habilita interrupciones de timer0
	*EXTMODE=1;      		   // bit0=1 para seleccionar int por flanco
	*EXTPOLAR=1;               // 1= int por flanco de subida, 0=por flanco de bajado
	*FIO3DIR=(11<<25);         // pines 25 y 26 del puerto 3 como salidas
	//*FIO0DIR=(1<<22);
	//*FIO3DIR=(1<<26);

	*FIO3SET=(1<<26);
	*FIO3SET=(1<<25);

	*T0MR0=25000000;		   //cargo el MR0 con 25M
	*T0MCR=0x00000001;		   //bit0=1 habilita interrupcion cdo matchea MR0 y TC
							   //bit1=1 resetea el TC cdo matchea MR0 y TC
	*T0TCR=0x00000001;		   //bit0=1 habilita el contador

}

void EINT0_IRQHandler(){
	int a=0;
	while(a<5000000){
		a++;
	}

	if(*FIO3PIN==0x00000000){
		*FIO3SET=(11<<25);
	}

	else{
		*FIO3CLR=(11<<25);
		//*FIO3CLR=(1<<26);
	}

	*EXTINT=1;
}

void TIMER0_IRQHandler(void){

	*T0IR=0x00000001; 	// (bit0= flag del MR0)
	*T0TC=0x00000000;

	if(*FIO0DIR==0x00400000){
		*FIO0DIR=0x00000000;
	}
	else{
		*FIO0DIR=0x00400000;
	}
}
