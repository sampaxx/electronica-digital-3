

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>

#define AddrISER0 0xE000E100     //Interrupt Set Enable Register
#define AddrT0TC 0x40004008      //T0TC - 0x4000 4008
#define AddrT0MR0 0x40004018   	 //T0MR0 - 0x4000 4018
#define AddrT0MCR 0x40004014	 //T0MCR - 0x4000 4014
#define AddrIO0IntEnF 0x40028094 //(IO0IntEnF - address 0x4002 8094)
#define AddrIO2IntEnF 0x400280B4 //(IO2IntEnF - 0x4002 80B4)
#define AddrT0MR1 0x4000401C     //T0MR1 - 0x4000401C
#define AddrT0IR 0x40004000      //T0IR - 0x40004000
#define AddrFIO3DIR 0x2009C060
#define AddrFIO3SET 0x2009C078 //FIO3SET - 0x2009 C078
#define AddrFIO3CLR 0x2009C07C
#define AddrFIO3PIN 0x2009C074
#define AddrT0TCR 0x40004004
#define AddrISER0 0xE000E100
#define AddrT0TCR 0x40004004
#define AddrIOIntStatus 0x40028080	//IOIntStatus - 0x4002 8080
#define AddrIO0IntClr 0x4002808C	//(IO0IntClr - 0x4002 808C)
#define AddrIO2IntClr 0x400280AC	//O2IntClr - 0x4002 80AC


unsigned int volatile *const T0TC=(unsigned int *const) AddrT0TC;
unsigned int volatile *const T0MR0=(unsigned int *const) AddrT0MR0;
unsigned int volatile *const T0MCR=(unsigned int *const) AddrT0MCR;
unsigned int volatile *const IO0IntEnF=(unsigned int *const) AddrIO0IntEnF;
unsigned int volatile *const IO2IntEnF=(unsigned int *const) AddrIO2IntEnF;
unsigned int volatile *const T0MR1=(unsigned int *const) AddrT0MR1;
unsigned int volatile *const T0IR=(unsigned int *const) AddrT0IR;
unsigned int volatile *const FIO3DIR=(unsigned int *const) AddrFIO3DIR;
unsigned int volatile *const FIO3SET=(unsigned int *const) AddrFIO3SET;
unsigned int volatile *const FIO3CLR=(unsigned int *const) AddrFIO3CLR;
unsigned int volatile *const FIO3PIN=(unsigned int *const) AddrFIO3PIN;
unsigned int volatile *const ISER0=(unsigned int *const) AddrISER0;
unsigned int volatile *const T0TCR=(unsigned int *const) AddrT0TCR;
unsigned int volatile *const IOIntStatus=(unsigned int *const) AddrIOIntStatus;
unsigned int volatile *const IO0IntClr=(unsigned int *const) AddrIO0IntClr;
unsigned int volatile *const IO2IntClr=(unsigned int *const) AddrIO2IntClr;


void config(void);

int main(void) {

	config();

    volatile static int i = 0 ;
    // Enter an infinite loop, just incrementing a counter
    while(1) {
        i++ ;
    }
    return 0 ;

}

void TIMER0_IRQHandler(){


	if(*T0IR==1){
		*FIO3SET=(11<<25);
	}
	if(*T0IR==2){
		*FIO3CLR=0xffffffff;

	if(*T0IR==1){				//interrumpio el MR0?
		*FIO3SET=0xffffffff;	//al hacer SET en el bit25 se paga el led
		*T0IR=1;				//bajo el flag de MR0

	}


	*T0IR=0xffffffff;

	if((*T0IR==2)||(*T0IR==3)){
		*T0IR=2;
		*FIO3CLR=0xFFFFFFFF;
	}

	if(*T0IR==3){
		*T0IR=2;
	}

//	if((*T0IR!=1)&&(*T0IR!=2)){
//		*T0IR=0xFFFFFFFF;
//	}

}

void EINT3_IRQHandler(void){

	//RSI GPIO0
	if(*IOIntStatus==0b1){
		*T0MR0=(*T0MR0<<1);
		//*T0MR1=(*T0MR1<<1);
	}

	//RSI GPIO2
	if(*IOIntStatus==0b100){
		*T0MR0=(*T0MR0>>1);
	}

	//se limpian los flags de cualquier interrupcion de GPIO 0 y 2
	*IO0IntClr=0xFFFFFFFF;
	*IO2IntClr=0xFFFFFFFF;
}

void config(){

	//config entradas del puerto 3
	*FIO3DIR=!(1<<10);

	//limpio FIO3PIN
	*FIO3PIN=0;

	//habilito int por timer0
	*ISER0=(1<<1);

	//limpio T0IR
	*T0IR=0xffffffff;

	//encendido del timer
	*T0TCR=1;

	*ISER0=(0x00040002);       // el bit18=1 habilita interrupciones externas por EINT0
							   // el bit1=1 habilita interrupciones de timer0
	*ISER0|=(1<<21);

	//enciendo el timer0
	*T0TCR=1;

	//-------------------------------------------------------------------------------------
	//config del MCR

	*T0MCR=0;		//limpio el MCR


	//config int por Timer0

	*T0MCR=(1<<0); //habilito int por T0-MR0
	*T0MCR|=(1<<3); //habilito int por T0-MR1
	*T0MCR|=(1<<4); //habilito habilito TC reset por matchear T0-MR1

	*T0MCR=1; //habilito int por T0-MR0

	*T0MCR|=(0b11<<3); //habilito int por T0-MR1


	//cargo el t0-MR0
	*T0MR0=40;//10 mseg
	//cargo el t0-MR1
	*T0MR1=9500;//20 mseg

	//config int por GPIO0 p0.0 por flanco de bajada
	*IO0IntEnF=1; //escribo el primer bit del registro.

	//config int por GPIO2 p2.0 por flanco de bajada
	*IO2IntEnF=1; //escribo el bit0 del registro

	// pin 25 del puerto 3 como salida
	*FIO3DIR=(1<<25);
	//*FIO0DIR=(1<<22);
	//*FIO3DIR=(1<<26);

	*FIO3CLR=(1<<25);		   //al inicio los led empezaran encendido
	//*FIO3SET=(1<<25);

}


