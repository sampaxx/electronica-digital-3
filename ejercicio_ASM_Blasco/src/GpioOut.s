.syntax unified
.global gpio_out
.type gpio_out, function

gpio_out:	LDR	R1,FIO3DIR		//cargo en r1 la direccion de memoria de la etiqueta FIO3DIR
			STR	R0,[R1]			// *r1=r0

			LDR R2,FIO0DIR		//CARGO R2 CON LA DIRECCION DE MEMORIA QUE VOY A TRABAJAR
			MOV	R0,0x00000000	//cargo r0 con 0
			STR R0,[R2]			//

			MOV pc, lr			// pc=lr-1

FIO0DIR:		.word	0x2009C000
FIO3DIR:		.word	0x2009C060
.end
