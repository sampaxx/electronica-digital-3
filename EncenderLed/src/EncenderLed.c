/*
===============================================================================
 Name        : EncenderLed.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>

// TODO: insert other include files here

// TODO: insert other definitions and declarations here
void init(void);

#define ledr 7

int main(void) {

    // TODO: insert code here
 init();
    // Force the counter to be placed into memory
    //volatile static int i = 0 ;
    // Enter an infinite loop, just incrementing a counter
    while(1) {
    	int i=0;
        for (i=0; i<10000000; i++) {
        }

        LPC_GPIO2->FIOSET=(1<<ledr);

        for(i=0; i<10000000; i++){
        }
        LPC_GPIO2->FIOCLR=(1<<ledr);

        }
    return 0;
}



void init (void){
	LPC_GPIO2->FIODIR |= (1<<ledr); // se define al pin p2.7 (llamado ledr) como salida
}
