.syntax unified
.global display
.type display, function

display:
	LDR	r1,=tabla7seg
	ADD	r0,r1,r0,LSL #2
	LDR	r0,[r0]				//Ahora en r0 tengo el
	LDR	R1,FIOCLR			//valor correspondiente de la tabla.
	MOV	R2,valor			//Apago leds que puedan haber
	STR	R2,[R1]				//quedado encendidos al convertir otro numero.
	LDR	r1,FIOSET			//Enciendo los segmentos para formar
	STR	r0,[r1]				//el numero deseado.
	//------------------------------------------------------------------------
	PUSH {r0}
	POP	 {r0}
	//------------------------------------------------------------------------
	MOV pc,lr

FIOCLR:		.word	0x2009c01c
FIOSET:		.word	0x2009c018
valor:		.word	0x07078004

tabla7seg:		.word	0x07018004			//0
uno:			.word	0x06000000			//1
dos:			.word	0x05030004			//2
tres:			.word	0x07020004			//3
cuatro:			.word	0x06028000			//4
cinco:			.word	0x03028004			//5
seis:			.word	0x03038004			//6
siete:			.word	0x06000004			//7
ocho:			.word	0x07038004			//8
nueve:			.word	0x07028004			//9
	.end
