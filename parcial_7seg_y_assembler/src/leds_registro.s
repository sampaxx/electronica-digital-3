.syntax unified
.global leds_registro
.type leds_registro, function

leds_registro:
bucle:
BL		ESPERA
LDR		R1,FIO0CLR
MOV		R2,#0x3
STR 	R2,[R1]
BL		ESPERA
AND 	R2,R0,#1
LDR		R1,FIO0SET
STR		R2,[R1]	//PRENDO EL LED DEL BIT CORRESPONDIENTE
MOV		R2,#0x2
STR 	R2,[R1] //PRENDO EL LED INDICADOR
LSR		R0, R0, #1
B		bucle



ESPERA:
		MOV		R2,#10
LAZO1:	SUBS	R2,R2,#1
		BNE		LAZO1
		MOV		PC,LR

FIO0SET:	.word	0x2009c018
FIO0CLR:	.word 	0x2009c01c
FIO0PIN: 	.word 	0x2009c014
